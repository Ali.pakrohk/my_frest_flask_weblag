# Imports Lib
from flask import Blueprint
# Set Blueprint Urls
admin = Blueprint('admin', __name__, url_prefix='/admin/')
# Admin Urls and Views
from .view import index
