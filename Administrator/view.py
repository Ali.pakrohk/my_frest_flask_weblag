from flask import session , render_template , request , abort
from Users.forms import LoginForm
from .  import admin


# Functions
@admin.route('/')
def index():
    return render_template('Administrator/Index.html')

@admin.route('/login/', methods=['GET','POST'])
def login():
    form = LoginForm(request.form)
    if request.method == 'POST' :
        if not form.validate_on_submit():
            abort(401)
    return render_template('Administrator/Login.html', form=form)
