# imports
from master import app
from flask import render_template
# Functions
@app.route('/')
def index():
    return render_template('Index.html')
