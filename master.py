# Imports
from Users import users
from Administrator import admin
# import views
from veiw import index
# Imports Python Moduls
from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from config import Development
# Application Settings
app = Flask(__name__)
app.config.from_object(Development)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
# Register Model & Blueprints
app.register_blueprint(admin)
app.register_blueprint(users)

